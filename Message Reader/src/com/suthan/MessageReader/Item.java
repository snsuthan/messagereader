package com.suthan.MessageReader;

import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class Item implements Comparable<Item>{
        private String name;
        private String data;
        private String mode;
        private String path;
        private String image;
       
        public Item(String n,String d, String p, String img,String mode)
        {
                name = n;
                data = d;
                path = p;
                image = img; 
                this.mode=mode;
        }
        public String getName()
        {
                return name;
        }
        public String getData()
        {
                return data;
        }
       
        public String getMode() {
			return mode;
		}
		public String getPath()
        {
                return path;
        }
        public String getImage() {
                return image;
        }
        @SuppressLint("DefaultLocale")
		public int compareTo(Item o) {
                if(this.name != null)
                        return this.name.toLowerCase().compareTo(o.getName().toLowerCase());
                else
                        throw new IllegalArgumentException();
        }
}