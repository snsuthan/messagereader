package com.suthan.MessageReader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;



public class ReadListCache extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "MessageReader";
    private static final int DATABASE_VERSION = 34;
	
	
	Context c;

	public ReadListCache(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		c = context;
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL("CREATE TABLE if not exists messageReadList(messege_id text PRIMARY KEY NOT NULL) ");
		
		

	}
	
	public void addMessage(String messge_id){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues message = new ContentValues();
		message.put("messege_id",messge_id);
		db.insert("messageReadList", null, message);
		db.close();
		
	}
	
	
	public Boolean checkReadMode(String message_id){
		Boolean mode=false;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from messageReadList WHERE messege_id = ?", new String[] {message_id});
		if (cursor.getCount() != 0) {
			mode=true;
		}
		cursor.close();
		db.close();
		return mode;
			
	}
	
	
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onCreate(db);
		
	}
	
	
}
