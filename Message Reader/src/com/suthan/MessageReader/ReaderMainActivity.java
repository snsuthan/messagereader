package com.suthan.MessageReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.TextView.BufferType;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.code.regexp.*;

public class ReaderMainActivity extends  Activity {

    private File currentDir;
    private ReaderArrayAdapter adapter;
	private ListView lv;
	TextView info_text;
	private String sender_name;
	private String phone_no;
	private String date;
	private String msg_body;
	private String file_name;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.main);
        
       lv=(ListView) findViewById(R.id.file_list_view);
       info_text=(TextView) findViewById(R.id.info_text);
       
       lv.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position,
				long arg3) {
			// TODO Auto-generated method stub
			
			 Item o = adapter.getItem(position);
             if(o.getImage().equalsIgnoreCase("directory_icon")||o.getImage().equalsIgnoreCase("directory_up")){
                             currentDir = new File(o.getPath());
                             fill(currentDir);
             }
             else
             {
                     onFileClick(o);
             }
			
			
			
		}
	});
        
        currentDir = new File("/mnt/");
        LogIn();
    }
    
    
    
    private void fill(File f)
    {
    	
    	
        File[]dirs = f.listFiles();
                TextView header=(TextView) findViewById(R.id.header);
                header.setText("Current Dir : "+f.getPath());
                 List<Item>dir = new ArrayList<Item>();
                 List<Item>fls = new ArrayList<Item>();
                 try{
                         for(File ff: dirs)
                         {
                        	
                                if(ff.isDirectory()){
                                       
                                       
                                        File[] fbuf = ff.listFiles();
                                        int buf = 0;
                                        if(fbuf != null){
                                                buf = fbuf.length;
                                        }
                                        else buf = 0;
                                        
                                        String num_item = String.valueOf(buf);
                                        
                                        if(buf == 0) num_item = num_item + " item";
                                        else num_item = num_item + " items";
                                       
                                        
                                        
                                        if((f.getName().equalsIgnoreCase("mnt") && ff.getName().endsWith("ard")) || !f.getName().equalsIgnoreCase("mnt")){
                                        	
                                        	//String formated = lastModDate.toString();
                                            dir.add(new Item(ff.getName(),num_item,ff.getAbsolutePath(),"directory_icon",""));
                                        }
                                }
                                else
                                {
                                	
                                	if(ff.getPath().substring(ff.getPath().lastIndexOf(".")+1).equals("vmg")){
                                		
                                		ReadListCache readListCache=new ReadListCache(getApplicationContext());
                                		
                                		fls.add(new Item(ff.getName(),ff.length() + " Bytes", ff.getAbsolutePath(),"file_icon",readListCache.checkReadMode(ff.getName())?"1":"0"));
                                	}
                                	
                                	
                                        
                                }
                         }
                 }catch(Exception e)
                 {   
                         
                 }
                 
                
                 Collections.sort(dir);
                 Collections.sort(fls);
                 dir.addAll(fls);
                 
                 if(!f.getName().equalsIgnoreCase("mnt"))
                         dir.add(0,new Item("..","Parent Directory",f.getParent(),"directory_up",""));
                 
                 
            	 
                 
                 
                 if(dir.size()<2 && !f.getName().equalsIgnoreCase("mnt")){
                	 info_text.setVisibility(View.VISIBLE);
                	 info_text.setText("No item");
                	 
                 }else{
                	 info_text.setVisibility(View.GONE);
                 }
                 
                 adapter = new ReaderArrayAdapter(ReaderMainActivity.this,R.layout.file_view,dir);
                 lv.setAdapter(adapter); 
                 
    }
    
   
    private void onFileClick(Item o)
    {
    	
    	String input_text="";
    	
    	file_name=o.getName();
    	
		
    	try {
			input_text=getStringFromFile(o.getPath());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	Pattern p = Pattern.compile(".*CHARSET=UTF-8:;*(?<sender_name>\\w*);*.*TEL;.*(?<phone_no>\\d{9,11})." +
    			"*Date:(?<date>\\d{2}.\\d{2}.\\d{4}\\s\\d{2}.\\d{2}.\\d{2})." +
    			"*QUOTED-PRINTABLE:(?<msg_body>.*)..END:VBODY.*");
    	
    	
    	Matcher m = p.matcher(input_text);
    	if(m.matches()){
    		
    		sender_name=m.group("sender_name");
    		phone_no=m.group("phone_no");
    		date=m.group("date");
    		msg_body=m.group("msg_body");
    		
    		
    		Pattern qp_p = Pattern.compile("(=[0-9A-Z]{2})");
    		Matcher qp_m= qp_p.matcher(msg_body);
    		
    		 while (qp_m.find()){
         		
         			msg_body=msg_body.replace(qp_m.group(0),Character.toString((char) Integer.parseInt(qp_m.group(0).substring(1),16)));
         			qp_m = qp_p.matcher(msg_body);
    			 
    		 }
    		
    		msg_body=msg_body.replaceAll("=..","").replaceAll("[\\\\][n]","");
    		
    		
    		readOneMessageDialog();
    	}
    	
    	
    	
        
    }
    
    
    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
          sb.append(line).append("\\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile (String filePath) throws Exception {
        File fl = new File(filePath);
       
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();        
        return ret;
    }
    
    
    
    public static CharSequence addSmileySpans(Context ch, CharSequence recieved_message)
    {
        

    final HashMap<String, Integer> smilyRegexMap = new HashMap<String, Integer>();
    
            smilyRegexMap.put( ":-\\)" , R.drawable.emo_im_happy);
            smilyRegexMap.put( ":-\\(" , R.drawable.emo_im_sad);
            smilyRegexMap.put( ";-\\)" , R.drawable.emo_im_winking);
            smilyRegexMap.put( ":-[pP]" , R.drawable.emo_im_tongue_sticking_out);
            smilyRegexMap.put( ":-[O]",R.drawable.emo_im_surprised);
            smilyRegexMap.put( ":-\\*" , R.drawable.emo_im_kissing);
            smilyRegexMap.put( ":[O]", R.drawable.emo_im_yelling);
            
            
            
            smilyRegexMap.put( "[B]-\\)" , R.drawable.emo_im_cool);
            smilyRegexMap.put( ":-\\$" , R.drawable.emo_im_money_mouth);
            smilyRegexMap.put( ":-\\!" , R.drawable.emo_im_foot_in_mouth);
            smilyRegexMap.put( ":-\\[" , R.drawable.emo_im_embarrassed);
            smilyRegexMap.put( "[O]:-\\)",R.drawable.emo_im_angel);
            smilyRegexMap.put( ":-\\\\" , R.drawable.emo_im_undecided);
            smilyRegexMap.put( ":'\\(", R.drawable.emo_im_crying);
            
            
            
            smilyRegexMap.put( ":-[X]" , R.drawable.emo_im_lips_are_sealed);
            smilyRegexMap.put( ":-[D]" , R.drawable.emo_im_laughing);
            smilyRegexMap.put( "[o]_[O]" , R.drawable.emo_im_wtf);
            smilyRegexMap.put( "\\<[3]" , R.drawable.emo_im_heart);
            smilyRegexMap.put( "[x]-\\(",R.drawable.emo_im_mad);
            smilyRegexMap.put( ":-[/]" , R.drawable.emo_im_smirk);
            smilyRegexMap.put( ":-[|]", R.drawable.emo_im_pokerface);
            
            
            




       
        SpannableStringBuilder builder = new SpannableStringBuilder(recieved_message);

        @SuppressWarnings("rawtypes")
        Iterator it = smilyRegexMap.entrySet().iterator();
        while (it.hasNext()) {
            @SuppressWarnings("rawtypes")
            Map.Entry pairs = (Map.Entry) it.next();

            Pattern mPattern = Pattern.compile((String) pairs.getKey());
            Matcher matcher = mPattern.matcher(recieved_message);

            while (matcher.find()) {

                    Bitmap smiley = BitmapFactory.decodeResource(ch.getResources(), ((Integer) pairs.getValue()));
                    
                    Object[] spans = builder.getSpans(matcher.start(), matcher.end(), ImageSpan.class);
                    if (spans == null || spans.length == 0) {
                        builder.setSpan(new ImageSpan(ch,smiley), matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
            }
        }
        return builder;
    }
    
    
    
    public void readOneMessageDialog(){
    	
    	final Dialog message_dialog = new Dialog(ReaderMainActivity.this);
       	message_dialog.setTitle("Message View");
       	message_dialog.setContentView(R.layout.message_view_dialog);
       	
 	    	Button ok_btn =(Button) message_dialog.findViewById(R.id.ok_btn);
 	    	
 	    	
 	    	TextView phone_no_tv = (TextView) message_dialog.findViewById(R.id.phone_no);
 	    	TextView name_tv = (TextView) message_dialog.findViewById(R.id.name);
 	    	TextView date_tv = (TextView) message_dialog.findViewById(R.id.date);
 	    	TextView msg_body_tv = (TextView) message_dialog.findViewById(R.id.msg_body);
 	    	
 	    	phone_no_tv.setText(phone_no);
 	    	name_tv.setText(sender_name);
 	    	date_tv.setText(date);
 	    	msg_body_tv.setText( addSmileySpans(getApplicationContext(),msg_body), BufferType.SPANNABLE );
 	    	
 	    	
 	    	ok_btn.setOnClickListener(new View.OnClickListener() {

 	            @Override
 	            public void onClick(View v) {
 	                // TODO Auto-generated method stub
 	            	
 	            	ReadListCache readListCache=new ReadListCache(getApplicationContext());
 	            	readListCache.addMessage(file_name);
 	            	 fill(currentDir);
 	            	message_dialog.cancel();
 	            }
 	        });
 	    	
 	    	
 	    	
 	    	
 	    	message_dialog.show();
    	
    	
    }
    
    
    
public void LogIn(){
    	
    	final Dialog message_dialog = new Dialog(ReaderMainActivity.this);
       	message_dialog.setTitle("Log In Please...");
       	message_dialog.setContentView(R.layout.logindialog);
       	
 	    	Button ok_btn =(Button) message_dialog.findViewById(R.id.pin_btn);
 	    	
 	    	final EditText editText=(EditText) message_dialog.findViewById(R.id.login_pin);
 	    	
 	    	ok_btn.setOnClickListener(new View.OnClickListener() {

 	            @Override
 	            public void onClick(View v) {
 	                // TODO Auto-generated method stub
 	            	if(editText.getText().toString().trim().equals(getString(R.string.password))){
 	            		 fill(currentDir);
 	 	            	message_dialog.cancel();
 	            	}else{
 	            		editText.setText("");
 	            		editText.requestFocus();
 	            		Toast.makeText(getApplicationContext(), "Enter The Correct Pin Number", Toast.LENGTH_SHORT).show();
 	            	}
 	            	
 	            }
 	        });
 	    	
 	    	
 	    	
 	    	
 	    	message_dialog.show();
    	
    	
    }
    
    
    
    
    
    
    
    
    
}