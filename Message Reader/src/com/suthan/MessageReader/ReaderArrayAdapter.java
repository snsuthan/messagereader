package com.suthan.MessageReader;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ReaderArrayAdapter extends ArrayAdapter<Item>{

        private Context c;
        private int id;
        private List<Item>items;
       
        public ReaderArrayAdapter(Context context, int textViewResourceId,
                        List<Item> objects) {
                super(context, textViewResourceId, objects);
                c = context;
                id = textViewResourceId;
                items = objects;
        }
        public Item getItem(int i)
         {
                 return items.get(i);
         }
         @Override
       public View getView(int position, View convertView, ViewGroup parent) {
               View v = convertView;
               if (v == null) {
                   LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                   v = vi.inflate(id, null);
               }
              
               /* create a new view of my layout and inflate it in the row */
                //convertView = ( RelativeLayout ) inflater.inflate( resource, null );
               
               final Item o = items.get(position);
               if (o != null) {
                       TextView t1 = (TextView) v.findViewById(R.id.TextView01);
                       TextView t2 = (TextView) v.findViewById(R.id.TextView02);
                       TextView read_mode = (TextView) v.findViewById(R.id.read_mode);
                       /* Take the ImageView from layout and set the city's image */
                                ImageView imageCity = (ImageView) v.findViewById(R.id.fd_Icon1);
                                String uri = "drawable/" + o.getImage();
                           int imageResource = c.getResources().getIdentifier(uri, null, c.getPackageName());
                           Drawable image = c.getResources().getDrawable(imageResource);
                           imageCity.setImageDrawable(image);
                      
                       if(t1!=null)
                       t1.setText(o.getName());
                       if(t2!=null)
                       t2.setText(o.getData());
                       String temp_mode="";
                       if(o.getMode().equals("1")){
                    	   temp_mode="Already View" ;
                    	   read_mode.setTextColor(Color.BLUE);
                       }else if(o.getMode().equals("0")){
                    	   
                    	   temp_mode="Not Yet View" ;
                    	   read_mode.setTextColor(Color.RED);
                       }
                       
                       read_mode.setText(temp_mode);
                       
                       
                       
               }
               return v;
       }
}